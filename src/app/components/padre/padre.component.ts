import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  constructor() { }

  primerMensaje! : string;
  segundoMensaje! : string;
  tercerMensaje!: string;
  cuartoMensaje!: string;
  quintoMensaje!: string;


  recibirMensaje ($event: string): void {
    this.primerMensaje = $event;
    console.log(this.primerMensaje);  
  }

  recibirSegundo ($event: string): void {
    this.segundoMensaje = $event;
  }

  recibirTercer ($event: string): void {
    this.tercerMensaje = $event;
  }

  recibirCuarto($event: string): void {
    this.cuartoMensaje = $event;
  }

  recibirQuinto ($event: string): void {
    this.quintoMensaje = $event;
  }

  ngOnInit(): void {
  }

}

